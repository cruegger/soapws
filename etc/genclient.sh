#!/bin/bash 
wsdl2java -p com.agilex.orderservice.client  -frontend jaxws21 http://localhost:8080/SoapWeb/ws/OrderService?wsdl

wsimport -keep -p com.rueggerllc.client.order -target 2.0 http://captain:8086/soapws/ws/OrderService?wsdl

wsimport -keep -p com.rueggerllc.client.hello -target 2.0 http://captain:8086/soapws/ws/HelloService?wsdl
