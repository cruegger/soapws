package com.rueggerllc.services.order;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebParam.Mode;
import javax.jws.WebService;


@WebService(name="OrderService", targetNamespace="http://rueggerllc.com/services")
public interface OrderService {
	
  @WebMethod(operationName="processOrder")
  public OrderResponse processOrder(@WebParam(targetNamespace = "http://rueggerllc.com/services", name = "request", mode = Mode.IN)
                                              OrderRequest request);
  
  
  @WebMethod(operationName="sayHello")
  public String sayHello(String theData);
		                     
}
