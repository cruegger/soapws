package com.rueggerllc.services.order;

public class DeleteOrderResponse {
	
  private String confirmationNumber;

  public String getConfirmationNumber() {
	  return confirmationNumber;
  }

  public void setConfirmationNumber(String confirmationNumber) {
	  this.confirmationNumber = confirmationNumber;
  }
  
}
