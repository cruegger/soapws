package com.rueggerllc.services.order;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebParam.Mode;
import javax.jws.WebService;

import org.apache.log4j.Logger;

@WebService(endpointInterface="com.rueggerllc.services.order.OrderService",
		    targetNamespace="http://rueggerllc.com/services",
		    serviceName="OrderService",
		    portName="OrderServicePort")
		    
public class OrderServiceImpl implements OrderService {
	
	private static Logger logger = Logger.getLogger(OrderServiceImpl.class);
	
 
	@WebMethod(operationName="processOrder")
	public OrderResponse processOrder(@WebParam(targetNamespace="http://rueggerllc.com/services",name="request", mode=Mode.IN)
			OrderRequest request) {
		logger.info("ProcessOrder BEGIN");
		logger.info("Request ORDER ID = " + request.getOrderId());
		OrderResponse response = new OrderResponse();
		response.setConfirmationNumber("85443");
		logger.info("ProcessOrder END");
		return response;
	}

	public String sayHello(String theData) {
		logger.info("-----SAY HELLO: " + theData);
		logger.info("--Here is a change made on Ubuntu!!");
		return "Foobar";
	}

	@WebMethod(operationName="deleteOrder")
	public DeleteOrderResponse deleteOrder(@WebParam(targetNamespace="http://rueggerllc.com/WS",name="request", mode=Mode.IN)
			DeleteOrderRequest request) {
		logger.info("deleteOrder BEGIN");
		logger.info("Order ID=" + request.getOrderId());
		DeleteOrderResponse response = new DeleteOrderResponse();
		response.setConfirmationNumber("12356");
		logger.info("deleteOrder END");
		return response;
	}
  
}
