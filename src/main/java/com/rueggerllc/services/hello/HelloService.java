package com.rueggerllc.services.hello;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService(name="HelloService", targetNamespace="http://rueggerllc.com/services")
public interface HelloService {

	@WebMethod(operationName="sayHello")
	String sayHello();

	@WebMethod(operationName="inq")
	HelloInqResponse inq(@WebParam(name="request")HelloInqRequest request);

	@WebMethod(operationName="add")
	String add();

	@WebMethod(operationName="update")
	String update();

}
