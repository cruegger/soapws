package com.rueggerllc.services.hello;

import org.apache.log4j.Logger;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(
    endpointInterface = "com.rueggerllc.services.hello.HelloService",
    targetNamespace = "http://rueggerllc.com/services",
    serviceName = "HelloService",
    portName = "HelloServicePort")
public class HelloServiceImpl implements HelloService {

  private static Logger logger = Logger.getLogger(HelloServiceImpl.class);

  public String sayHello() {
    logger.info("sayHello BEGIN");
    return "Hello World JAX-WS";
  }

  public HelloInqResponse inq(@WebParam(name="request")HelloInqRequest request) {
    logger.info("inq() BEGIN");
    logger.info("name=" + request.getName());
    logger.info("customerId=" + request.getCustomerId());
    HelloInqResponse response = new HelloInqResponse();
    response.setMessage(String.format("Inq Response message: You passed name: %s", request.getName()));
    response.setVersion("1.0.0");
    return response;
  }

  public String add() {
    logger.info("add() BEGIN");
    return "Add Response";
  }

  public String update() {
    logger.info("update() BEGIN");
    return "Update Response";
  }

}
