FROM tomcat:8.0.46-jre8
RUN mkdir /usr/local/tomcat/webapps/myapp
COPY /target/soapws.war /usr/local/tomcat/webapps
EXPOSE 8080